from django.apps import AppConfig


class ApiappConfig(AppConfig):
    name = 'ApiAPP'
