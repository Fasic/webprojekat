
from django.http import HttpResponse, JsonResponse
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt

from WebAPP.models import Task, Label



def get_task(request, id):
    try:
        task = Task.objects.get(pk=id)
        return getJsonReturnCode(200, task.to_json())
    except:
        getJsonReturnCode(400)
    return getJsonReturnCode(200)

def get_all_tasks(request):
    ret = []
    for i in Task.objects.all():
        ret.append(i.to_json())
    return getJsonReturnCode(200, ret)

@csrf_exempt
def add_task(request):
    data = request.body.strip()
    task = Task().from_json(data)
    task.save()
    return getJsonReturnCode(200, task.to_json())

@csrf_exempt
def done_task(request, id):
    try:
        task = Task.objects.get(pk=id)
        if(task.state == 2):
            task.state = 1
            task.save()
            return getJsonReturnCode(200)
        else:
            return getJsonReturnCode(304)
    except:
        getJsonReturnCode(400)
    return getJsonReturnCode(200)


def delete_task(request, id):
    try:
        task = Task.objects.get(pk=id)
        task.delete()
        return getJsonReturnCode(200)
    except:
        getJsonReturnCode(400)
    return getJsonReturnCode(200)


def add_user(request):
    return None


def add_category(request):
    return None


def add_label(request):
    return None


def getJsonReturnCode(code, data=None):
    response = HttpResponse(
        JsonResponse(
            data=data,
            safe=False),
        content_type="application/json")
    response.status_code = code
    return response


