from django.urls import path
from . import views

urlpatterns = [
    path('task/get/<int:id>', views.get_task, name='get_task'),
    path('task/get_all', views.get_all_tasks, name='get_all_tasks'),
    path('task/add', views.add_task, name='add_task'),
    path('task/done/<int:id>', views.done_task, name='done_task'),
    path('task/delete/<int:id>', views.delete_task, name='delete_task'),

    path('user/add', views.add_user, name='add_user'),
    path('category/add', views.add_category, name='add_category'),
    path('label/add', views.add_label, name='add_label'),
]

#127.0.0.1:8000/api/task/delete/1
"""
Dodavanje:
    -USER
    -CATEGORY
    -LABEL
Izmena:
    -TASK-a
    -USER
    -CATEGORY
    -LABEL
DELETE, brisanje:
    -USER
    -CATEGORY
    -LABEL
    
Dodavanje LABEL-a TASK-u!!!

"""