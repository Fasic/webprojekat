from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('WebAPP.urls')), #HTML
    path('api/', include('ApiAPP.urls')), #API
]


