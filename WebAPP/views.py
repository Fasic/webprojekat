from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render, get_object_or_404
from django.urls import reverse

from WebAPP.models import Task, User, Category, Label


def index(request):
    tasks = Task.objects.order_by('-state','-priority')
    return render(request, 'tasks.html', {"tasks": tasks})


def task_done(request, id):
    task = get_object_or_404(Task, pk=id)

    task.state = 1
    task.save()

    return HttpResponseRedirect(reverse('index'))

def task_delete(request, id):
    task = get_object_or_404(Task, pk=id)
    task.delete()
    return HttpResponseRedirect(reverse('index'))

def task_add(request):
    if request.method == 'POST':
        task = Task(
            name=request.POST['name'],
            state=request.POST['state'], #1 ili 2
            user_id=request.POST['user']
        )

        desc = request.POST['desc']
        if(desc != "" and desc != None):
            task.desc = desc

        try:
            prio = int(request.POST['prio'])
            if (prio >= 1 and prio <= 10):
                task.priority = prio
        except:
            print()


        try:
            cat = int(request.POST['category'])
            if (cat != -1):
                task.category_id = cat
        except:
            print()
        task.save()


        for key in request.POST:
            if(str(key).startswith("label")):
                l1 = Label.objects.get(pk=request.POST[key])
                task.labels.add(l1)


        return HttpResponseRedirect(reverse('index'))
    else:

        users = User.objects.all()
        categorys = Category.objects.all()
        labels = Label.objects.all()
        context = {
            "users": users,
            "categorys": categorys,
            "labels": labels
        }

        return render(request, 'add_task.html', context)


