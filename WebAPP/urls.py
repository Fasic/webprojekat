from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('task/add', views.task_add, name='task_add'),

    path('task/done/<int:id>', views.task_done, name='task_done'),
    path('task/delete/<int:id>', views.task_delete, name='task_delete'),
]