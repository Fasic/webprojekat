import json

from django.db import models

class User(models.Model):
    user_name = models.CharField(max_length=200)

    def __str__(self):
        return self.user_name

    def to_json(self):
        dictionary = dict(
            id=self.id,
            user_name=self.user_name
        )
        return dictionary

    def from_json(self, json_data):
        data = json.loads(json_data)
        user = User()
        user.user_name = data["user_name"]
        return user

class Category(models.Model):
    name = models.CharField(max_length=200)
    icon = models.CharField(max_length=500, blank=True, null=True)
    color = models.CharField(max_length=10)

    def __str__(self):
        return self.name + " (#" + self.color + ")"

    def to_json(self):
        dictionary = dict(
            id=self.id,
            name=self.name,
            icon=self.icon,
            color=self.color
        )
        return dictionary

    def from_json(self, json_data):
        data = json.loads(json_data)
        cate = Category()
        cate.name = data["name"]
        cate.icon = data["icon"]
        cate.color = data["color"]
        return cate


class Label(models.Model):
    name = models.CharField(max_length=200)
    color = models.CharField(max_length=10)

    def __str__(self):
        return self.name

    def to_json(self):
        dictionary = dict(
            id=self.id,
            name=self.name,
            color=self.color
        )
        return dictionary

    def from_json(self, json_data):
        data = json.loads(json_data)
        label = Label()
        label.name = data["name"]
        label.color = data["color"]
        return label



class Task(models.Model):
    name = models.CharField(max_length=200)
    desc = models.CharField(max_length=2000, blank=True, null=True)
    state = models.IntegerField(default=2) # -1. Deleted, 0. zaledjen task, 1. Done, 2. Not Done (napravljen)
    priority = models.IntegerField(default=5) # 1 najmanja vaznost, 10 najveca vaznost

    user = models.ForeignKey(User, on_delete=models.CASCADE)
    category = models.ForeignKey(Category, on_delete=models.CASCADE, blank=True, null=True)
    labels = models.ManyToManyField(Label, blank=True, null=True)


    def __str__(self):
        return self.name

    def to_json(self):
        label_array = []
        for i in self.labels.all():
            label_array.append(i.to_json())

        if(self.category):
            cat = self.category.to_json()
        else:
            cat = {}

        dictionary = dict(
            id=self.id,
            name=self.name,
            desc=self.desc,
            state=self.state,
            priority=self.priority,
            user=self.user.to_json(),
            category=cat,
            labels=label_array
        )
        return dictionary

    def from_json(self, json_data):
        data = json.loads(json_data)
        task = Task()
        task.name = data["name"]
        task.desc = data["desc"]
        task.state = data["state"]
        task.priority = data["priority"]
        task.user_id = data["user_id"]
        if(data["category_id"] != -1):
            task.category_id = data["category_id"]
        return task


# RESTAPI - GET POST
# JSON
