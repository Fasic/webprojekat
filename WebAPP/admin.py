from django.contrib import admin

# Register your models here.
from WebAPP.models import User, Category, Task, Label

admin.site.register(User)
admin.site.register(Category)
admin.site.register(Task)
admin.site.register(Label)